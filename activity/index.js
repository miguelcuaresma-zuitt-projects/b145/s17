console.log("Hello World")

function calculator() {
	let alertBtn = ""
	let calculatorPrompt1 = parseInt(prompt("Please provide a number:"))
	let calculatorPrompt2 = parseInt(prompt("Please provide another number:"))
	let total = calculatorPrompt1 + calculatorPrompt2 
	console.log(typeof total)
/*	let difference = calculatorPrompt1 - calculatorPrompt2
	let product = calculatorPrompt1 * calculatorPrompt2
	let quotient = calculatorPrompt1 / calculatorPrompt2*/
	if (total <= 10) {
		total = calculatorPrompt1 + calculatorPrompt2
		alertBtn = total
		console.warn("The Sum of those numbers is: " + alertBtn)
	} else if (total <= 11 && 20) {
		total = calculatorPrompt1 - calculatorPrompt2
		alertBtn = total
		alert("The difference of those numbers is: " + alertBtn)
	} else if (total <= 29) {
		total = calculatorPrompt1 * calculatorPrompt2
		alertBtn = total
		alert("The product of those numbers is: " + alertBtn)
	} else {
		total = calculatorPrompt1 / calculatorPrompt2
		alertBtn = total
		alert("The quotient of those numbers is: " + alertBtn)
	}
}

function ageNameChecker() {
	let alertAge = ""
	let namePrompt = prompt("What is your name?")
	let agePrompt = parseInt(prompt("How about your age?"))
	let nameAndAge = namePrompt + agePrompt
	if ((namePrompt === null || namePrompt.trim() === "") || (agePrompt === null || agePrompt == undefined)) {
		alert("Are you a time traveller?") }
		else {

			alert(namePrompt + " is " + agePrompt + " years old")
		}
	}

function survey() {
	let input = parseInt(prompt("Provide an age."))
	switch (input) {
		case 18 :
			alert("You are now allowed to party.")
			break
		case 21 :
			alert("You are now part of the adult society.")
			break
		case 65 :
			alert("We thank you for your contribution to society.")
			break	
		default:
			alert("Are you sure you're not an alien?")
	}
}

function ageChecker() {
	// "value" - describes the value property of our elements
	let userAge = document.getElementById("age").value
	let ageOutput = document.getElementById("outputDisplay")
	try {
		// lets make sure that the input by the user is NOT equals to a blank string.
		//throw - this statement examines the input and returns an error.
		//console.log(typeof userAge)
		if (userAge === "") throw "The input is empty.";
		if (userAge === 0) throw "Invalid age"
		if (isNaN(userAge)) throw "The input is not a number."
		if (userAge <= 7) throw "The input is good for preschool"
		if (userAge > 7) throw "The input is too old for preschool"
		
	} catch(err) {
			// the "err" is to define the error that will be thrown by the  try section. so the "err" is caught by the catch statement and a custom error message will be displayed.
			ageOutput.innerHTML = "Age input: " + err
	} finally {
		alert("This is from the finally section")
	}	
}